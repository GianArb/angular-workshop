# angular-workshop — AngularJS Workshop App

Clonare git repo:

```
git clone https://bitbucket.org/corley/angular-workshop.git
cd angular-workshop
```

### Installare le dipendenze

Eseguire il comando:

```
npm install
```

### Avviare e testare l'app:

Eseguire il comando

```
npm start
```

Quindi aprire il browser e navigare `http://localhost:8000/app/index.html`.
Se l'ambiente è correttamente configurato, un messaggio di conferma verrà visualizzato.

In caso contrario contattare gli organizzatori del corso. Il corretto settaggio dell'ambiente è requisito fondamentale per accedere al corso.


[git]: http://git-scm.com/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://pivotal.github.com/jasmine/
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
[http-server]: https://github.com/nodeapps/http-server